<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="java.util.List"%>
<%@ page import="com.hcl.project.entities.*"%>


<!DOCTYPE html>

<html>

<head>

<meta charset="ISO-8859-1">

<title>Profile Page</title>


</head>

<body>
	<div align=center>

		<%
		String name = (String) request.getAttribute("name");
		session.setAttribute("name", name);
		
		
		%>

	
		
		<h1 style="font-size: 80px">Welcome To Surabi</h1>
		
		<h1>Hello <%=name%></h1>

		<p>
			
			<a href="logout" style="font-size: 30px">Logout </a>
		</p>

		<div align=center>

			<form method="post" action="saveMenu">

				<table border="1" style="background-color: #BDB76B">

					<tr>
						<td>SR_NO</td>
						<td>Item_ID</td>
						<td>Item</td>
						<td>Total_Price</td>
						<td>Quantity</td>
						
					</tr>


					<%
					@SuppressWarnings("unchecked")
					List<Menu> menu = (List<Menu>) request.getAttribute("list");
					%>

					<%
					int i =1;
					for (Menu m : menu) {
					%>

					<tr>
						<td><%=i++%>
					     <td><%=m.getItemid()%></td>
					     <td><%=m.getItem()%></td>
					     <td><%=m.getTotal_price()%></td>
					     <td><%=m.getQuantity()%></td>
						
						
					</tr>
					<%
					}
					%>
				</table>
			</form>
		</div>
	</div>
</body>
</html>
package com.hcl.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.project.entities.Menu;

public interface MenuRepo extends JpaRepository<Menu, Integer> {


}
